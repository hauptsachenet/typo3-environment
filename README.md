
## Local requirements

- docker 17.06.0-ce or better
- docker-compose (capable of reading v2 docker-compose files)
- GNU make
- rsync (for deployment or push/pulling of user data)

## Create a project
Create a project like this:

- `composer init --no-interaction --name="$(basename $PWD)"`
- `composer config platform.php 7.1.6` (or any other php version >= 7.0)
- `composer config extra.typo3/cms.web-dir web` (or any other web dir other than ".")
- `composer require hn/typo3-environment typo3/cms-core:8.7.*` and answer all questions
- run `make serve` and you should be in a running typo3 project
- if you are in the backend, you'll want to replace the typo3 console typoscript with the typoscript from the generated template extension 
- also, you'll want to select a backend layout ~ there is one generated for you via the template extension 

## Make commands you might want to use

- `make serve` will start the project, open the default browser and throw you onto the project
- `make start` will just start the server without the browser bit
- `make stop` stops the local servers
- `make cc` clears all typo3 caches and updates the database to the current schema required by all extensions
- `make clean` resets the local installation to zero.
- `make pull` clones all user data from a remote installation
- `make deploy` installs the project and moves all none-user-files to the `DEPLOY_HOST` and `DEPLOY_PATH`.

One nice thing is that you can chain make commands easily like `make clean pull serve` to clean your setup, install all dependencies again, start the webserver, pull all userdata from your live server and the open your webbrowser and throw you on the startpage.

## Configure deployment

There is an option `deployments` in the composer.json. Just add the following configuration:

```JSON
{
    "deployments": {
        "master": {
            "path": "html/master",
            "php_cli": "php_cli",
            "branch": "master",
            "host": "p000000@p000000.mittwaldserver.info"
        }
    }
}
```

The first deployment will be the default target. There isn't any implementation for multiple deployment targets yet. The default deployment will be added to your Makefile.

On your target server, you might want to define a few environment variables. For most hosters, like mittwald, it is the easiest to just define the environment using php. If your deployment path is `html/master` add a file named `html/master/shared/config/AdditionalConfiguration.php`.

```PHP
<?php

putenv('MYSQL_HOST=localhost');
putenv('MYSQL_USERNAME=username');
putenv('MYSQL_PASSWORD=password');
putenv('MYSQL_DATABASE=database');

putenv('IMAGE_PROCESSOR=ImageMagick');
putenv('IMAGE_PROCESSOR_COLORSPACE=sRGB');
putenv('IMAGE_PROCESSOR_PATH=/usr/bin/');
```

## What it does

- Create a docker environment
    - prepare an apache container with mod php with a configurable version (although not tested with php lower than 7.0)
    - prepare an mysql container with a configurable version (5.5 - 5.7)
    - put typo3temp into a tmpfs to work around the weak filesystem performance on docker for mac
- create an AdditionalConfiguration with
    - allow defining mysql access using environment
    - allow defining mailer configuration though environment
    - define image processing based on the environment
    - set the mysql timezone equal to the php timezone
    - configure some extensions that have bad default configuration
    - enable debug configuration in development context
    - less wasteful caching configuration using apcu if available and the filesystem if not
    - disable a lot of caches in dev context so that the constant cache clear trip isn't needed
- create a gitignore file
    - ignore the default typo3 folders
    - ignore extensions in typo3conf based on your installed composer libraries
- create a .htaccess (for apache 2.4+) with
    - useful deflate configuration for most basic text files
    - useful caching headers for anything not produced by php
    - set the security headers X-Content-Type-Options and X-Xss-Protection
    - set X-Frame-Options SAMEORIGIN to prevent clickjacking attacks
    - set X-UA-Compatible header for older ie's
    - set TYPO3_CONTEXT based on the hostname used for the request
    - configure the index.php as FallbackResource to allow usage of realurl (horrible extension but necessary until typo3 9.3)
- create a makefile for easy usage
    - includes a install start serve and stop commands to start and stop a local dev environment without much hassle
    - include a cc (cache clear) command to avoid the annoying trip though the install tool 
    - include a deploy command to easily create a versioned remote version
        - create date versioned deploys for easy rollback
        - exclude all testing resources from all composer libraries
        - configure rsync to reuse files from the last deploy for very quick deployment
        - create shared directories based on a configuration in the composer.json (which is also reflected into gitignore)
        - update database schema and run upgrade wizards
        - cleanup old releases
    - include a pull command to sync a remote userdata with the local version
        - sync all shared files
        - sync database but exclude some specific tables like cache tables or session tables
- create phpstorm meta information ~ to make the extbase development a little less annoying
- create a base extension to directly start coding which contains
    - a basic typoscript template
        - configure typolink to use absolute urls and set target="_blank" for external links
        - configure the rel="noopener" attribute to all links to prevent hyjacking and increase chrome performance
        - enable realurl by default ~ if available
        - configure typo3 cache to expire at midnight to have a consistent site
        - configure the default language to be english and enable all overlay options for a quick jump into multi language typo3 instances
        - disable all caches when the typo3 context is development
        - enable all caches and css/js compression when the typo3 context is production
        - configure a page object that uses the backend layout to find the right FluidTemplate
    - create tsconfig
        - with a basic backend layout
        - disable most basic content elements for you to enable when needed
    - create a main page tempalate to start templating right away
- add deployment unsing the makefile
    - production ready dependency installation
    - excluding of test files
    - optimized rsync's for quick transfers and not tripping the opcache
    - sharing of user files (like fileadmin) between releases (additional folders can be configured using a generator)
- add bower to the project (optionally)
    - configure the .bowerrc to point into the web directoy
    - configure .htaccess to cache bower components for an appropriate time 
    
I also want to add that most of the features are configurable.
You don't want a template extension? no problem.
To find out what is configurable check the composer.json file after you required the environment.
A few questions will also be asked during the first installation.

## What it does after the initial setup

Yes, this plugin will watch over your dependencies and has some tricks to help you after the initial setup

- If you install any extension it'll ask you if you want a local modify extension (eg. for news it'll ask you if it should generate a hn_news extension) wich will already be configured to overwrite the templates of the original extension.
- Adjust gitignore and the extension configuration of realurl if you install it
- Add libraries and extensions to the gitignore so you can share the typo3conf/ext folder with them
- Ongoing support, since this plugin can update your config files your typo3 configuration can improve over time with just a `composer update`

## Other things I want to add later

- allow adding own generators (like the ones within the plugin) within the project or other extensions
- support translate-locallang extension directly with shared l10n folder 
- generating a bitbucket pipeline for automatic deployment ~ but can be added manually if needed
- create local news extension if the news extension gets installed
- create local solr extension if the solr extension gets installed
- create server configuration for static file cache
- add configuration for an https redirect if needed
- add server configuration to block php access for files like favicon.ico for increased performance
- allow easy creation of more local extensions using a make command
- generate lint commands to enforce good programming habits ~ nice in combination with bitbucket pipelines
- maybe even auto generate a basic test framework

