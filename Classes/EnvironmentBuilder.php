<?php

namespace Hn\Typo3Environment;

use Composer\Composer;
use Composer\Config\JsonConfigSource;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Plugin\PluginInterface;
use Composer\Repository\RepositoryInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnvironmentBuilder implements PluginInterface, EventSubscriberInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        // nothing to do here
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     * * The method name to call (priority defaults to 0)
     * * An array composed of the method name to call and the priority
     * * An array of arrays composed of the method names to call and respective
     *   priorities, or 0 if unset
     *
     * For instance:
     *
     * * array('eventName' => 'methodName')
     * * array('eventName' => array('methodName', $priority))
     * * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            ScriptEvents::POST_UPDATE_CMD => 'executeFileGenerators',
            ScriptEvents::POST_INSTALL_CMD => 'executeFileGenerators',
        ];
    }

    /**
     * The plugin can run without the extension being installed.
     * To prevent this it must be checked that the extension is still installed.
     *
     * @param RepositoryInterface $repository
     * @return bool
     */
    protected function isInstalled(RepositoryInterface $repository): bool
    {
        foreach ($repository->getPackages() as $package) {
            if ($package->getName() === 'hn/typo3-environment') {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Event $event
     */
    public function executeFileGenerators(Event $event)
    {
        if (!$this->isInstalled($event->getComposer()->getRepositoryManager()->getLocalRepository())) {
            // the environment isn't locally installed
            // this may happen when you swtich branches etc.
            return;
        }

        $generators = new GeneratorContainer($event->getComposer(), $event->getIO());

        // configure phase

        $optionResolver = new OptionsResolver();
        $generators->configureOptions($optionResolver);
        $options = $this->resolveAndSaveOptions($optionResolver, $event->getComposer(), $event->getIO());

        // TODO remove this pseudo-option eventually
        $options['web_dir'] = $generators->getWebDir();

        $generators->execute($options);
    }

    /**
     * @param OptionsResolver $optionResolver
     * @param Composer $composer
     * @param IOInterface $io
     * @return mixed
     */
    private function resolveAndSaveOptions(OptionsResolver $optionResolver, Composer $composer, IOInterface $io)
    {
        $extras = $composer->getPackage()->getExtra();
        $originalOptions = isset($extras['hauptsache/typo3-environment']) ? $extras['hauptsache/typo3-environment'] : [];

        $options = $optionResolver->resolve($originalOptions);
        ksort($options);

        if ($originalOptions !== $options) {
            $extras['hauptsache/typo3-environment'] = $options;
            $composer->getPackage()->setExtra($extras);
            $composerFile = new JsonFile(trim(getenv('COMPOSER')) ?: 'composer.json', null, $io);
            $configSource = new JsonConfigSource($composerFile);
            $configSource->addProperty('extra.hauptsache/typo3-environment', $options);
        }

        return $options;
    }
}
