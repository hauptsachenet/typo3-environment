<?php

namespace Hn\Typo3Environment;


use Composer\Autoload\ClassMapGenerator;
use Composer\Composer;
use Composer\IO\IOInterface;
use Hn\Typo3Environment\Generator\GeneratorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeneratorContainer
{
    const PREPARE_PENDING = 0;
    const PREPARE_RUNNING = 1;
    const PREPARE_DONE = 2;

    /**
     * @var Composer
     */
    private $composer;

    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var array
     */
    private $generatorClasses;

    /**
     * @var GeneratorInterface[]
     */
    private $instances = [];

    /**
     * @var array GeneratorInterface[]
     */
    private $prepared = [];

    /**
     * @var array|null
     */
    private $options = null;

    public function __construct(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;

        $map = ClassMapGenerator::createMap(__DIR__ . '/Generator', '#Generator/.*/#');
        $this->generatorClasses = array_filter(array_keys($map), function ($className) {
            return is_subclass_of($className, GeneratorInterface::class, true);
        });
    }

    public function getComposer(): Composer
    {
        return $this->composer;
    }

    public function getIo(): IOInterface
    {
        return $this->io;
    }

    public function getUnprepared(string $className): GeneratorInterface
    {
        if (isset($this->instances[$className])) {
            return $this->instances[$className];
        }

        if (!in_array($className, $this->generatorClasses)) {
            throw new \RuntimeException("Generator $className does not exist");
        }

        /** @var GeneratorInterface $instance */
        $instance = new $className($this);
        $this->instances[$className] = $instance;
        $this->prepared[$className] = self::PREPARE_PENDING;
        return $instance;
    }

    public function has(string $className): bool
    {
        if ($this->options === null) {
            throw new \RuntimeException("Options must be set");
        }

        $isEnabled = $this->options[$className] ?? true;
        if ($isEnabled === false) {
            return false;
        }

        return true;
    }

    public function get(string $className): GeneratorInterface
    {
        if ($this->options === null) {
            throw new \RuntimeException("Options must be set");
        }

        if ($this->prepared[$className] === self::PREPARE_RUNNING) {
            throw new \RuntimeException("Recursive dependency involving $className");
        }

        if (!$this->has($className)) {
            throw new \RuntimeException("Generator $className is disabled.");
        }

        $generator = $this->getUnprepared($className);
        if ($this->prepared[$className] !== self::PREPARE_DONE) {
            $generator->prepare($this->options);
            $this->prepared[$className] = self::PREPARE_DONE;
        }

        return $generator;
    }

    public function configureOptions(OptionsResolver $optionResolver)
    {
        foreach ($this->generatorClasses as $generatorClass) {
            $optionResolver->setDefined($generatorClass);
            $optionResolver->setAllowedTypes($generatorClass, ['bool']);
            $this->getUnprepared($generatorClass)->configureOptions($optionResolver);
        }
    }

    public function execute(array $options)
    {
        if ($this->options !== null) {
            throw new \RuntimeException("Generators are currently being executed.");
        }
        $this->options = $options;

        $generators = [];
        foreach ($this->generatorClasses as $generatorClass) {
            if ($options[$generatorClass] ?? true) {
                $generators[] = $this->get($generatorClass);
            }
        }

        foreach ($generators as $generator) {
            $generatorClass = get_class($generator);
            if ($this->io->isVerbose()) {
                $this->io->write("execute <info>$generatorClass</info>");
            }
            $generator->execute($options);
        }

        if (!$this->io->isVerbose()) {
            $count = count($generators);
            $this->io->write("executed <info>$count</info> generators. Use -v to see which generators get executed.");
        }

        $this->options = null;
    }

    public function getOptions(): array
    {
        if ($this->options === null) {
            throw new \RuntimeException("Options aren't defined yet");
        }

        return $this->options;
    }

    public function getOption(string $name)
    {
        return $this->options[$name];
    }

    public function getWebDir()
    {
        $extra = $this->getComposer()->getPackage()->getExtra();
        return isset($extra['typo3/cms']['web-dir'])
            ? rtrim($extra['typo3/cms']['web-dir'], '/')
            : '.';
    }
}