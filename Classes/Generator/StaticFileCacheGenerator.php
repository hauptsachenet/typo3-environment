<?php

namespace Hn\Typo3Environment\Generator;


use Composer\Package\PackageInterface;
use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaticFileCacheGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @return PackageInterface|null
     */
    public function getPackage()
    {
        $localRepository = $this->container->getComposer()->getRepositoryManager()->getLocalRepository();
        return $localRepository->findPackage('lochmueller/staticfilecache', '^5.0');
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $this->container->getUnprepared(ExtensionGenerator::class)->preventAsking('hn_staticfilecache');
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $package = $this->getPackage();
        if ($package === null) {
            return;
        }

        $extConf = $this->container->get(ExtConfGenerator::class);
        $extConf->addExtConf('staticfilecache', 'sendCacheControlHeader', '1', '0');
        $extConf->addExtConf('staticfilecache', 'fileTypes', 'xml,rss', '');
        $extConf->addExtConf('staticfilecache', 'disableInDevelopment', '0', '1');

        $htaccess = $this->container->get(HtaccessGenerator::class);
        $htaccess->addConfiguration(implode(PHP_EOL, [
            '# static file cache',
            'RewriteCond %{REQUEST_METHOD} GET', // only get requests
            'RewriteCond %{QUERY_STRING} ^$', // without a query string
            'RewriteCond %{HTTP_COOKIE} !be_typo_user [NC]', // without a backend user
            'RewriteCond %{HTTP_COOKIE} !nc_staticfilecache [NC]', // without a frontend user (special cookie from extension)
            'RewriteCond %{DOCUMENT_ROOT}/typo3temp/tx_staticfilecache/%{REQUEST_SCHEME}/%{HTTP_HOST}%{REQUEST_URI}/index.html -f', // if the file exists
            'RewriteRule .* typo3temp/tx_staticfilecache/%{REQUEST_SCHEME}/%{HTTP_HOST}%{REQUEST_URI}/index.html', // then rewrite
            'RewriteCond %{REQUEST_FILENAME} typo3temp/tx_staticfilecache.*.html', // if this is now a static file cache
            'RewriteCond %{HTTP:Accept-Encoding} gz [NC]', // and the browser accepts gzip
            'RewriteCond %{REQUEST_FILENAME}.gz -f', // and there is a gzip version
            'RewriteRule .* $0.gz [T=text/html]', // then use the gzip version of the file instead
            "Header merge Vary Accept-Encoding 'expr=%{REQUEST_FILENAME} =~ m#typo3temp/tx_staticfilecache#'", // tell proxies that accept encoding is handled
            '<FilesMatch \.html\.gz$>',
            '    Header append Content-Encoding gzip', // tell the browser that the content is gzipped
            '    SetEnv no-gzip 1',
            '    ForceType text/html',
            '</FilesMatch>',
        ]));
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
    }
}