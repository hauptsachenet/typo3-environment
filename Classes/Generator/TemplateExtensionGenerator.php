<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateExtensionGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $extensionBuilder = $this->container->getUnprepared(ExtensionGenerator::class);
        $extensionBuilder->askForExtension('hn_template');
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $extensionBuilder = $this->container->get(ExtensionGenerator::class);
        $extensionBuilder->hintExtension('hn_template', 'ext_emconf', [
            'category' => 'fe'
        ]);

        $extensionBuilder->hintExtension('hn_template', 'ts_setup', [$this->getTyposcript($options)]);
        $extensionBuilder->hintExtension('hn_template', 'ts_config', [$this->getTsConfig($options)]);

        $extensionBuilder->hintExtension('hn_template', 'templates', [
            'Page/Main.html' => $this->getPageTemplate($options)
        ]);
        $extensionBuilder->hintExtension('hn_template', 'partials', [
            'Page/Header.html' => $this->getHeaderPartial($options),
            'Page/Footer.html' => $this->getFooterPartial($options)
        ]);
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
    }

    protected function getTyposcript(array $options): string
    {
        $locale = 'en_US.utf8';
        $language = substr($locale, 0, 2);
        $htmlLocale = strtr(substr($locale, 0, 5), '_', '-');
        $extKey = 'hn_template';
        $extensionName = str_replace('_', '', ucwords($extKey, '_'));

        return <<<TypoScript

config.absRefPrefix = /
config.extTarget = _blank
config.fileTarget = _blank
config.ATagParams = rel="noopener"
config.tx_realurl_enable = 1

# cache headers would cause the user to have an inconsistent experience
# you can clear the typo3 cache but you can't clear your users browser cache
config.sendCacheHeaders = 0

config.cache_clearAtMidnight = 1
config.cache_period = 86400

# remove default js (because it is inserted within the head = evil) but keep some spam protect
config.removeDefaultJS = 1
config.spamProtectEmailAddresses = ascii

# default language
config.sys_language_uid = 0
config.language = $language
config.sys_language_isocode_default = $language
config.locale_all = $locale
config.htmlTag_langKey = $htmlLocale
config.sys_language_mode = content_fallback
# uncomment if you have problems with the wrong language being loaded
# overlay shouldn't be necessary in typo3 8
# enabling this will reduce performance drastically
# config.sys_language_overlay = hideNonTranslated

# cache and compression
[applicationContext = Development]
    config.no_cache = 1
    config.concatenateCss = 0
    config.concatenateJs = 0
    config.compressCss = 0
    config.compressJs = 0
    config.contentObjectExceptionHandler = 0
[else]
    config.no_cache = 0
    config.concatenateCss = 1
    config.concatenateJs = 1
    config.compressCss = 1
    config.compressJs = 1
    config.contentObjectExceptionHandler = 1
[end]

page = PAGE
page {
    typeNum = 0
    
    meta {
        viewport = width=device-width, initial-scale=1
        keywords.field = keywords
        description.field = description
        format-detection = telephone=no
        robots = noindex, nofollow
        robots.stdWrap.if.isTrue.data = page:no_search
    }
    
    10 = FLUIDTEMPLATE
    10 {
        templateName.cObject = TEXT
        templateName.cObject {
            value = pagets__Main
            override.data = levelfield:-1, backend_layout_next_level, slide
            override.override.field = backend_layout
            
            # convert something like "pagets__Main" to "Main"
            replacement.10 {
                search = pagets__
                replace = 
            }
        }
        templateRootPaths {
            10 = EXT:fluid_styled_content/Resources/Private/Templates
            20 = EXT:$extKey/Resources/Private/Templates/Page
        }
        layoutRootPaths {
            10 = EXT:fluid_styled_content/Resources/Private/Layouts
            20 = EXT:$extKey/Resources/Private/Layouts
        }
        partialRootPaths {
            10 = EXT:fluid_styled_content/Resources/Private/Partials
            20 = EXT:$extKey/Resources/Private/Partials
        }
        extbase.controllerExtensionName = $extensionName
    }
}

lib.contentElement {
    templateRootPaths.10 = EXT:hn_template/Resources/Private/Templates/ContentElement
    layoutRootPaths.10 = EXT:hn_template/Resources/Private/Layouts
    partialRootPaths.10 = EXT:hn_template/Resources/Private/Partials
}
TypoScript;
    }

    public function getTsConfig(array $options): string
    {
        return <<<TsConfig
TCEFORM.tt_content.CType.removeItems = bullets, header, image, text, textpic, media, multimedia, search, uploads, table, div

mod.web_layout.BackendLayouts {
    Main {
        title = Main
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = main
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }
}
TsConfig;
    }

    public function getPageTemplate(array $options): string
    {
        return <<<FluidTemplate
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" data-namespace-typo3-fluid="true">

<f:render partial="Page/Header" arguments="{_all}"/>

<main>
    <f:cObject typoscriptObjectPath="styles.content.get"/>
    <f:comment>
        I recommend using the vhs view helper if you need multiple columns.
        https://fluidtypo3.org/viewhelpers/vhs/4.1.0/Content/GetViewHelper.html
        You might need to run "composer require fluidtypo3/vhs" first.
    </f:comment>
</main>

<f:render partial="Page/Footer" arguments="{_all}"/>

</html>
FluidTemplate;
    }

    public function getHeaderPartial(array $options): string
    {
        return <<<FluidTemplate
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" data-namespace-typo3-fluid="true">



</html>
FluidTemplate;
    }

    public function getFooterPartial(array $options): string
    {
        return <<<FluidTemplate
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" data-namespace-typo3-fluid="true">

<div id="footer">© {f:format.date(date: 'now', format: 'Y')}</div>

</html>
FluidTemplate;
    }
}