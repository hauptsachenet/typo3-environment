<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class SetupGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $make = $this->container->get(MakefileGenerator::class);

        $localconf = escapeshellarg(Path::join($options['web_dir'], 'typo3conf/LocalConfiguration.php'));
        $installCmd = '$(LOCAL_TYPO3CMS) install:setup --site-setup-type=site';
        $make['start']->addCommand('if [ ! -f ' . $localconf . ' ]; then ' . $installCmd . '; fi', -1);
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
    }
}