<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhpStormMetaGenerator implements GeneratorInterface
{
    /**
     * @var MarkerArea
     */
    private $marker;

    /**
     * @var GeneratorContainer
     */
    private $container;

    public function __construct(GeneratorContainer $container)
    {
        $this->marker = new MarkerArea(get_class($this));
        $this->container = $container;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function prepare(array $options)
    {
    }

    public function execute(array $options)
    {
        if (!file_exists('.phpstorm.meta.php')) {
            file_put_contents('.phpstorm.meta.php', "<?php\nnamespace PHPSTORM_META;\n");
        }

        $this->marker->replaceInFile('.phpstorm.meta.php', <<<'PHPSTORM_META'

// Hint that makeInstance will return an instance of the class it gets passed
override(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(0), type(0));

// Hint that object manager will return an instance of the class it gets passed
override(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface::get(0), type(0));

// requires the deep-assoc-completion extension
$GLOBALS['TSFE'] = new \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController();
$GLOBALS['TYPO3_DB'] = new \TYPO3\CMS\Core\Database\DatabaseConnection();
$GLOBALS['BE_USER'] = new \TYPO3\CMS\Core\Authentication\BackendUserAuthentication();

PHPSTORM_META
        );
    }
}