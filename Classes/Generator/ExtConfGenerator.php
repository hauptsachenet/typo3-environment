<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class ExtConfGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var MarkerArea
     */
    private $marker;

    /**
     * @var array
     */
    private $extConf = [];

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
        $this->marker = new MarkerArea(get_class($this));
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $this->addExtConf('scheduler', 'maxLifetime', '1440', '60');
    }

    public function addExtConf(string $extKey, string $key, $original, $value)
    {
        if (!isset($this->extConf[$extKey])) {
            $this->extConf[$extKey] = [];
        }

        if (!isset($this->extConf[$extKey][$key])) {
            $this->extConf[$extKey][$key] = [];
        }

        $this->extConf[$extKey][$key][] = [$original, $value];
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
        $content = [];

        foreach ($this->extConf as $extKey => $configuration) {
            $search = [];
            $replace = [];
            foreach ($configuration as $key => $pairs) {
                foreach ($pairs as $pair) {
                    $search[] = substr(serialize([$key => $pair[0]]), 5, -1);
                    $replace[] = substr(serialize([$key => $pair[1]]), 5, -1);
                }
            }
            $variable = "\$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][" . var_export($extKey, true) . "]";
            $searchPhp = var_export(count($search) > 1 ? $search : reset($search), true);
            $replacePhp = var_export(count($replace) > 1 ? $replace : reset($replace), true);
            $content[] = "if (isset($variable)) {\n$variable = str_replace(\n$searchPhp,\n$replacePhp,\n$variable);\n}";
        }

        $filename = Path::join($options['web_dir'], 'typo3conf/AdditionalConfiguration.php');
        $this->marker->replaceInFile($filename, implode(PHP_EOL, $content), "<?php\n");
    }
}