<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\OptionsResolver;

interface GeneratorInterface
{
    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container);

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver);

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options);

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options);
}