<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class BowerGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        // ask if this generator should be active
        $resolver->setDefault(get_class($this), function (Options $options) {
            return $this->container->getIo()->askConfirmation("Use bower? (default yes) ", true);
        });
    }

    private function getBowerDirectory(): string
    {
        if (file_exists('.bowerrc')) {
            $bowerrc = json_decode(file_get_contents('.bowerrc'), true);
            return $bowerrc['directory'] ?? 'bower_components';
        }

        return Path::join($this->container->getWebDir(), 'bower_components');
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $bowerDirectory = $this->getBowerDirectory();

        if ($this->container->has(GitIgnoreGenerator::class)) {
            $gitignore = $this->container->get(GitIgnoreGenerator::class);
            $gitignore->addRule($bowerDirectory, true);
        }

        if ($this->container->has(MakefileGenerator::class)) {
            $make = $this->container->get(MakefileGenerator::class);
            $make->setEnvironment('LOCAL_BOWER', 'docker run --rm -v$(CURDIR):$(CURDIR) -w$(CURDIR) digitallyseamless/nodejs-bower-grunt:5 bower');
            $make['install']->addConditionalCommand(
                'ifeq ($(ENVIRONMENT), development)',
                '$(LOCAL_BOWER) install',
                '$(LOCAL_BOWER) install --production'
            );
        }

        if ($this->container->has(HtaccessGenerator::class)) {
            $htaccess = $this->container->get(HtaccessGenerator::class);
            $pathRegex = '^/' . Path::makeRelative($bowerDirectory, $this->container->getWebDir()) . '/';
            $htaccess->addExpiresToPath($pathRegex, HtaccessGenerator::CACHE_TIME_MUTABLE);
            $htaccess->addBlockPath($pathRegex);
        }
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
        $jsonOptions = JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT;

        if (!file_exists('.bowerrc')) {
            $bowerrc = ['directory' => $this->getBowerDirectory()];
            file_put_contents('.bowerrc', json_encode($bowerrc, $jsonOptions));
        }

        if (!file_exists('bower.json')) {
            $packageName = $this->container->getComposer()->getPackage()->getName();
            $bowerJson = [
                'name' => strtolower(preg_replace('#^.*/#', '', $packageName)),
                'private' => true,
                'dependencies' => []
            ];
            file_put_contents('bower.json', json_encode($bowerJson, $jsonOptions));
        }
    }
}