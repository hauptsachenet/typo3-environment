<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\Generator\Makefile\Target;
use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MakefileGenerator implements GeneratorInterface, \ArrayAccess
{
    const POSITION_INIT = 20;
    const POSITION_TOP = 10;
    const POSITION_NORMAL = 0;
    const POSITION_END = -10;
    const POSITION_CLEANUP = -20;

    /**
     * @var MarkerArea
     */
    private $marker;

    /**
     * @var array
     */
    private $environment = [];

    /**
     * @var Target[]
     */
    private $targets = [];

    /**
     * @var GeneratorContainer
     */
    private $container;

    public function __construct(GeneratorContainer $container)
    {
        $this->marker = new MarkerArea(get_class($this));
        $this->container = $container;

        // create the basic targets
        $this['.PHONY']->addDependency($this['serve']);
        $this['.PHONY']->addDependency($this['start']);
        $this['.PHONY']->addDependency($this['install']);
        $this['.PHONY']->addDependency($this['cc']);
        $this['.PHONY']->addDependency($this['clean']);
        $this['.PHONY']->addDependency($this['stop']);

        // create serve and start in advance
        $this['serve']->addDependency($this['start']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function prepare(array $options)
    {
        $this->setEnvironment('LOCAL_COMPOSER', 'composer', false);
        $this->setEnvironment('LOCAL_TYPO3CMS', 'vendor/bin/typo3cms', false);
        $this->setEnvironment('ENVIRONMENT', 'development', false);

        $this['install']->addConditionalCommand(
            'ifeq ($(ENVIRONMENT), development)',
            '$(LOCAL_COMPOSER) install --dev',
            '$(LOCAL_COMPOSER) install --no-dev --prefer-dist --optimize-autoloader --classmap-authoritative'
        );

        $this['clean']->addDependency($this['stop']);

        $this['cc']->addDependency($this['start']);
        $this['cc']->addCommand('$(LOCAL_TYPO3CMS) database:updateschema --verbose', MakefileGenerator::POSITION_TOP);
        $this['cc']->addCommand('$(LOCAL_TYPO3CMS) cache:flush', MakefileGenerator::POSITION_TOP);
        $this['cc']->addCommand('$(LOCAL_TYPO3CMS) extension:setupactive', MakefileGenerator::POSITION_TOP);
    }

    public function execute(array $options)
    {
        $content = '';

        foreach ($this->environment as $name => $value) {
            $content .= "$name?=$value\n";
        }

        foreach ($this->targets as $target) {
            $name = $target->getName();

            foreach ($target->getEnvironment() as $key => $value) {
                $content .= "\n$name: $key=$value";
            }

            $content .= "\n" . $name . ':';

            foreach ($target->getDependencies() as $dependency) {
                $content .= " $dependency";
            }

            foreach ($target->getEnvironmentRequirement() as $variable => $errorMessage) {
                $content .= "\nifndef $variable\n\t$(error " . escapeshellarg($errorMessage) . ")\nendif";
            }

            $detailedCommands = $target->getDetailedCommands();

            $conditionGroups = [];
            foreach ($detailedCommands as $detailedCommand) {
                $index = $detailedCommand['priority'] . $detailedCommand['condition'];
                $conditionGroups[$index][] = $detailedCommand;
            }

            foreach ($conditionGroups as $conditionGroup) {
                $condition = reset($conditionGroup)['condition'];
                if (!$condition) {
                    foreach ($conditionGroup as $detailedCommand) {
                        $content .= "\n\t" . str_replace("\n", "\n\t", $detailedCommand['command']);
                    }
                } else {
                    $content .= "\n" . $condition;
                    foreach ($conditionGroup as $detailedCommand) {
                        if (!$detailedCommand['command']) {
                            continue;
                        }

                        $content .= "\n\t" . str_replace("\n", "\n\t", $detailedCommand['command']);
                    }
                    if (count(array_filter(array_column($conditionGroup, 'else')))) {
                        $content .= "\nelse";
                        foreach ($conditionGroup as $detailedCommand) {
                            if (!$detailedCommand['else']) {
                                continue;
                            }

                            $content .= "\n\t" . str_replace("\n", "\n\t", $detailedCommand['else']);
                        }
                    }
                    $content .= "\nendif";
                }
            }

            $content .= "\n\n";
        }

        $this->marker->replaceInFile('Makefile', $content);
    }

    public function setEnvironment(string $var, string $value, bool $override = true)
    {
        if ($override === true || !isset($this->environment[$var])) {
            $this->environment[$var] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->targets);
    }


    public function offsetGet($offset): Target
    {
        if (!isset($this->targets[$offset])) {
            $this->targets[$offset] = new Target($offset);
        }

        return $this->targets[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (!$value instanceof Target) {
            $type = is_object($value) ? get_class($value) : gettype($value);
            throw new \RuntimeException("Expected Target, got $type");
        }

        $this->targets[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->targets[$offset]);
    }
}