<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class HtaccessGenerator implements GeneratorInterface
{
    const CACHE_TIME_MUTABLE  = 'access plus 5 minutes';
    const CACHE_TIME_IMMUTABLE  = 'access plus 1 year';
    const COMPRESS_TYPES = [
        'text/plain',
        'text/html',
        'text/css',
        'text/csv',
        'application/xml',
        'application/rss+xml',
        'application/atom+xml',
        'application/javascript',
        'application/x-shockwave-flash',
        'application/json',
        'image/svg+xml',
        'font/ttf',
        'font/woff',
        'font/woff2',
        'font/otf',
    ];

    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var MarkerArea
     */
    private $marker;

    /**
     * @var string[][]
     */
    private $configurations = [];

    /**
     * @var array
     */
    private $cacheTimePaths = [];

    /**
     * Search bots
     *
     * @var array
     */
    private $searchAgents = [
        'aolbuild',
        'baidu',
        'bingbot|bingpreview|msnbot',
        'duckduckgo',
        'adsbot-google|googlebot|mediapartners-google',
        'teoma',
        'slurp',
        'yandex',
    ];

    /**
     * Files in this list won't be redirected to typo3 to handle.
     * However... if the file exists it will be delivered still.
     * Regex is supported
     *
     * @var array
     */
    private $blockList = [
        '^/fileadmin/',
        '^/typo3/',
        '^/typo3conf/',
        '^/typo3temp/',
        '^/uploads/',
        '^/wp-admin',
        '^/favicon.[a-z]{3,4}$',
        '^/apple-touch-icon',
        '^/autodiscover'
    ];

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
        $this->marker = new MarkerArea(get_class($this));
    }

    public function addConfiguration(string $configuration, string $condition = '')
    {
        if (!isset($this->configurations[$condition])) {
            $this->configurations[$condition] = [];
        }

        $this->configurations[$condition][] = $configuration;
    }

    public function addExpiresToPath(string $pathRegex, string $time = self::CACHE_TIME_MUTABLE)
    {
        if (!isset($this->cacheTimePaths[$time])) {
            $this->cacheTimePaths[$time] = [];
        }

        $this->cacheTimePaths[$time][] = $pathRegex;
    }

    public function addBlockPath(string ...$blockPaths)
    {
        array_push($this->blockList, ...$blockPaths);
    }

    public function addSearchAgent(string ...$searchAgents)
    {
        array_push($this->searchAgents, ...$searchAgents);
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'htaccess_domain_staging' => '\.hauptsache\.net$|test|alpha|beta|staging',
            'htaccess_domain_production' => '^www\.'
        ]);
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $this->addConfiguration("RewriteEngine on");
        $this->addConfiguration("Options -MultiViews -Indexes");

        $this->addTypo3Configuration($options);
        $this->addCompressionConfiguration($options);
        $this->addExpiresConfiguration($options);
        $this->addSecurityConfiguration($options);
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
        foreach ($this->cacheTimePaths as $time => $pathRegexs) {
            $condition = 'If "%{REQUEST_URI} =~ m#' . implode('|', $pathRegexs) . '#"';
            $this->addConfiguration('ExpiresActive on', $condition);
            $this->addConfiguration('ExpiresDefault "' . $time . '"', $condition);
        }

        if (!empty($this->blockList)) {
            sort($this->blockList);
            $this->addConfiguration('# prevent a fallback to typo3 for some urls');
            foreach ($this->blockList as $index => $item) {
                $isLast = $index + 1 === count($this->blockList);
                $modifier = $isLast ? '[NC]' : '[NC,OR]';
                $this->addConfiguration("RewriteCond %{REQUEST_URI} $item $modifier");
            }
            $this->addConfiguration('RewriteCond %{REQUEST_FILENAME} !-f');
            $this->addConfiguration('RewriteRule ^ - [R=404]');
        }

        if (!empty($this->searchAgents)) {
            sort($this->searchAgents);
            $this->addConfiguration('# block most search engines when not on the live domain');
            $this->addConfiguration('RewriteCond %{HTTP_HOST} ' . $options['htaccess_domain_production'] . ' [NC]');
            foreach ($this->searchAgents as $index => $item) {
                $isLast = $index + 1 === count($this->searchAgents);
                $modifier = $isLast ? '[NC]' : '[NC,OR]';
                $this->addConfiguration("RewriteCond %{HTTP_USER_AGENT} $item $modifier");
            }
            $this->addConfiguration('RewriteRule ^ - [R=403]');
        }

        $htaccessLocation = Path::join($this->container->getWebDir(), '.htaccess');

        $lines = [];
        foreach ($this->configurations as $condition => $configurations) {
            if ($condition) {
                $tagName = substr($condition, 0, strpos($condition, ' ') ?: null);
                $lines[] = "<$condition>";
                foreach ($configurations as $configuration) {
                    $lines[] = '    ' . $configuration;
                }
                $lines[] = "</$tagName>";
            } else {
                foreach ($configurations as $configuration) {
                    $lines[] = $configuration;
                }
            }
        }

        $this->marker->replaceInFile($htaccessLocation, implode(PHP_EOL, $lines));
    }

    protected function addSecurityConfiguration(array $options)
    {
        // nosniff prevents ie from guessing the mime type if the type from the server isn't matching
        // example: a user uploads a pdf with the file extension jpg and IE notices and opens the file as pdf
        $this->addConfiguration("Header always set X-Content-Type-Options 'nosniff'");

        // X-UA-Compatible is the same as the html header.
        // It tells IE to render with it's newest rendering engine even if the page isn't valid
        $this->addConfiguration("Header always set X-UA-Compatible 'IE=edge' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'");

        // This is a feature of Internet Explorer, Chrome and Safari
        // If a query parameter contains html and is rendered within the page it will be obscured by the browser
        // However: this header tells the browser to not only obscure the page but to block it entirely
        // this will tell the user that something is definitely wrong rather than showing them a somewhat broken page
        $this->addConfiguration("Header always set X-XSS-Protection '1; mode=block' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'");

        // prevent the page to be rendered within an iframe that is not from the same domain
        // this will help prevent click hijacking
        $this->addConfiguration("Header always merge X-Frame-Options 'SAMEORIGIN' 'expr=%{CONTENT_TYPE} =~ m#^text/html#'");
    }

    protected function addExpiresConfiguration(array $options)
    {
        // add the public cache control flag if the response isn't generated by php
        // this will tell proxies that they are allowed to cache the response
        // even if proxies aren't used, it'll tell the browser that it is allowed to cache the response in filesystem
        // private caches are only stored in memory to prevent some possible attacks
        // the keyword "always" is omitted om purpose since i don't want it on error pages and want it overridable by other means
        $this->addConfiguration("Header merge Cache-Control 'public' 'expr=%{REQUEST_FILENAME} !~ /\\.php\$/'");

        // only let the browser cache resources from which the url isn't important
        // fileadmin may change files and expect the browser to show new stuff
        // extension may update and expect the browser to show new stuff
        // favicon.ico is impossible to cache due to it's fixed name
        // however...
        // it's perfectly fine to cache compressed css files which's filename is a hash
        $this->addExpiresToPath('^/fileadmin/_processed_/', self::CACHE_TIME_IMMUTABLE);
        $this->addExpiresToPath('^/typo3temp/', self::CACHE_TIME_IMMUTABLE);

        // however... typo3 isn't helping much with some resource being included directly.
        // for that reason i add just a little bit of caching to the extension directory.
        $this->addExpiresToPath('^/typo3/sysext/', self::CACHE_TIME_MUTABLE);
        $this->addExpiresToPath('^/typo3conf/ext/', self::CACHE_TIME_MUTABLE);

        // prevent typo3 from overriding our configuration
        if ($this->container->has(ConfigGenerator::class)) {
            $config = $this->container->get(ConfigGenerator::class);
            $config->appendConfiguration("\$GLOBALS['TYPO3_CONF_VARS']['SYS']['generateApacheHtaccess'] = false;");
        }
    }

    protected function addCompressionConfiguration(array $options)
    {
        foreach (self::COMPRESS_TYPES as $mimeType) {
            $this->addConfiguration("AddOutputFilterByType DEFLATE $mimeType");
        }
    }

    /**
     * @param array $options
     */
    protected function addTypo3Configuration(array $options)
    {
        $this->addConfiguration("DirectoryIndex index.php");
        $this->addConfiguration('FallbackResource /index.php');

        // set typo3 environment based on the hostname
        $this->addConfiguration("SetEnv TYPO3_CONTEXT=Development");
        $this->addConfiguration("SetEnvIfNoCase Host {$options['htaccess_domain_staging']} TYPO3_CONTEXT=Production/Staging");
        $this->addConfiguration("SetEnvIfNoCase Host {$options['htaccess_domain_production']} TYPO3_CONTEXT=Production");
    }
}