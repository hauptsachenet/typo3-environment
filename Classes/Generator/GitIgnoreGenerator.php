<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Traversable;
use Webmozart\PathUtil\Path;

class GitIgnoreGenerator implements GeneratorInterface, \IteratorAggregate
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var MarkerArea
     */
    private $marker;

    /**
     * @var array
     */
    private $rules = [];

    public function __construct(GeneratorContainer $container)
    {
        $this->marker = new MarkerArea(get_class($this));
        $this->container = $container;
    }

    public function addRule(string $rule, bool $addToCleanCommand = false)
    {
        foreach ($this->rules as $existingRule) {
            // if this exclude path is already covered by another rule ignore it
            if (Path::isBasePath($existingRule, $rule)) {
                return;
            }

            // if another rule is more precise than the current one remove it
            if (Path::isBasePath($rule, $existingRule)) {
                unset($this->rules[$existingRule]);
            }
        }

        $this->rules[$rule] = $rule;

        if ($addToCleanCommand && $this->container->has(MakefileGenerator::class)) {
            $make = $this->container->get(MakefileGenerator::class);
            $make['clean']->addCommand('rm -rf ' . preg_replace('#[^a-zA-Z0-9,._+:@%/*-]#', '\\\\$0', $rule));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('ignore_local_configuration', function (Options $options) {
            // if you have deployment, then you most likely want to track the state
            if ($options[DeploymentGenerator::class] ?? true) {
                return false;
            }

            $question = "Should the local state (LocalConfiguration and PackageStates) be versioned? ";
            $question .= "(default yes; use no if you plan to never release this project eg. for a library) ";
            return !$this->container->getIo()->askConfirmation($question, true);
        });
    }

    public function prepare(array $options)
    {
        $webDir = $this->container->getWebDir();

        // os
        $this->addRule('.DS_Store');
        $this->addRule('.idea');

        // dependency management
        $this->addRule('vendor', true);

        // enable files
        $this->addRule(Path::join($webDir, 'FIRST_INSTALL'), true);
        $this->addRule(Path::join($webDir, 'typo3conf/ENABLE_INSTALL_TOOL'), true);

        // typo3 installer stuff
        $this->addRule(Path::join($webDir, 'index.php'));
        $this->addRule(Path::join($webDir, 'typo3'));
        $this->addRule(Path::join($webDir, 'typo3_src'));
        $this->addRule(Path::join($webDir, 'typo3_src-*'));

        // typo3 extensions and other installations outside of the vendor dir
        $installationManager = $this->container->getComposer()->getInstallationManager();
        $localRepository = $this->container->getComposer()->getRepositoryManager()->getLocalRepository();
        foreach ($localRepository->getCanonicalPackages() as $package) {
            $installPath = Path::makeRelative($installationManager->getInstallPath($package), getcwd());
            $this->addRule($installPath);
        }

        // temp files
        $this->addRule(Path::join($webDir, 'typo3temp/*'), true);
        $this->addRule(Path::join($webDir, 'typo3conf/temp_CACHED*'));
        $this->addRule(Path::join($webDir, 'typo3conf/deprecation_*.log'));

        if ($options['ignore_local_configuration']) {
            $this->addRule(Path::join($webDir, 'typo3conf/LocalConfiguration.php'), true);
            $this->addRule(Path::join($webDir, 'typo3conf/PackageStates.php'), true);
        }
    }

    public function execute(array $options)
    {
        sort($this->rules);
        $this->marker->replaceInFile('.gitignore', implode(PHP_EOL, $this->rules));
    }

    /**
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        foreach ($this->rules as $rule) {
            yield $rule;
        }
    }
}