<?php

namespace Hn\Typo3Environment\Generator;


use Composer\Package\CompletePackage;
use Composer\Package\Package;
use Composer\Repository\PlatformRepository;
use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Yaml\Yaml;
use Webmozart\PathUtil\Path;

class DockerGenerator implements GeneratorInterface
{
    const DOCKERFILE_PHP = 'Dockerfile-php';

    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var PlatformRepository
     */
    private $platformRepository;

    /**
     * @var array
     */
    private $packages = ['imagemagick'];

    /**
     * @var array
     */
    private $apacheModules = ['alias', 'deflate', 'expires', 'headers', 'rewrite'];

    /**
     * @var array
     */
    private $environment = [];

    /**
     * @var array
     */
    public $services = [];

    /**
     * @var array
     */
    public $volumes = [];

    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
        $platformOverrides = $this->container->getComposer()->getConfig()->get('platform');
        $this->platformRepository = new PlatformRepository([], $platformOverrides);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'db_port' => function (Options $options) {
                return $options['web_port'] + 1;
            },
            'db_version' => '5.7',
            'php_ini' => [
                'max_execution_time' => '240',
                'max_input_vars' => '1500',
                'opcache.enable_file_override' => 'On',
                'opcache.revalidate_freq' => '0'
            ],
            'php_environment' => [
                'PLATFORM' => 'Docker',
                'TYPO3_CONTEXT' => 'Development'
            ],
            'web_port' => function (Options $options) {
                return (int)$this->container->getIo()->ask("What port do you want to use for the webserver? (default 5000) ", 5000);
            }
        ]);

        $resolver->setAllowedTypes('db_port', 'int');
        $resolver->setAllowedTypes('web_port', 'int');
        $resolver->setNormalizer('db_version', function (Options $options, $value) {
            if (!preg_match('#5.[567](.\d+)?#', $value)) {
                throw new InvalidOptionsException("Only mysql 5.5 - 5.7 is currently supported");
            }

            return $value;
        });

        $resolver->setAllowedTypes('php_ini', 'array');
        $resolver->setNormalizer('php_ini', function (Options $option, $items) {
            ksort($items);
            return $items;
        });
    }

    public function defineService(string $name, array $service)
    {
        $this->services[$name] = array_merge_recursive($this->services[$name] ?? [], $service);
    }

    public function addVolume(string $name, array $config = [])
    {
        if (isset($this->volumes[$name])) {
            throw new \RuntimeException("Volume $name is already defined.");
        }

        $this->volumes[$name] = $config;
    }

    public function addPackage(string $package)
    {
        $this->packages[$package] = $package;
    }

    public function requireApacheModules(string $apacheModule)
    {
        $this->apacheModules[$apacheModule] = $apacheModule;
    }

    public function setEnvironment(string $key, string $value)
    {
        $this->defineService('php', ['environment' => ["$key=$value"]]);
        $this->environment[$key] = $value;
    }

    public function passEnvironment(string $key)
    {
        $this->defineService('php', ['environment' => ["$key"]]);
        $this->environment[$key] = null;
    }

    public function getPhpPackage(): Package
    {
        // TODO the php version must be persisted.

        /** @var CompletePackage $package */
        foreach ($this->platformRepository->findPackages('php') as $package) {
            return $package;
        }

        throw new \RuntimeException("php package not found");
    }

    public function prepare(array $options)
    {
        if ($this->container->has(MakefileGenerator::class)) {
            $makefile = $this->container->get(MakefileGenerator::class);

            $makefile->setEnvironment('LOCAL_DOCKER_COMPOSE', "docker-compose");
            $makefile->setEnvironment('LOCAL_OPEN_ADDRESS', "open");
            $makefile->setEnvironment('LOCAL_COMPOSER', "docker-compose run --rm --no-deps php composer");
            $makefile->setEnvironment('LOCAL_TYPO3CMS', 'docker-compose exec php vendor/bin/typo3cms');

            $makefile['install']->addCommandForEnvironment('development', '$(LOCAL_DOCKER_COMPOSE) build');

            $makefile['start']->addDependency($makefile['install']);
            $makefile['start']->addCommand('$(LOCAL_DOCKER_COMPOSE) up -d');
            $pollForMysql = 'while ! mysql -uroot -p$$MYSQL_ROOT_PASSWORD -e"SELECT 1" > /dev/null 2>&1; do echo -n .; sleep 1; done; echo mysql online';
            $makefile['start']->addCommand('$(LOCAL_DOCKER_COMPOSE) exec db bash -c ' . escapeshellarg($pollForMysql));

            $makefile['stop']->addCommand('$(LOCAL_DOCKER_COMPOSE) down');

            $makefile['serve']->addDependency($makefile['start']);
            $makefile['serve']->addCommand('$(LOCAL_OPEN_ADDRESS) http://localhost:' . $options['web_port'] . '/');

            $makefile['clean']->addDependency($makefile['stop']);
            $makefile['clean']->addCommand('$(LOCAL_DOCKER_COMPOSE) down -v');
        }

        // im kind of split if this config options belong into the config generator or here
        // the options itself are defined in the config generator and they would therefor belong there
        // however the values are partially depended on the docker environment and in general these settings wouldn't exist without docker

        $this->setEnvironment('MYSQL_DATABASE', 'database');
        $this->setEnvironment('MYSQL_USERNAME', 'root');
        $this->setEnvironment('MYSQL_PASSWORD', 'password');
        $this->setEnvironment('MYSQL_HOST', 'db');

        $this->passEnvironment('MAIL_TRANSPORT');
        $this->passEnvironment('MAIL_SENDMAIL_COMMAND');
        $this->passEnvironment('MAIL_SMTP_ENCRYPT');
        $this->passEnvironment('MAIL_SMTP_PASSWORD');
        $this->passEnvironment('MAIL_SMTP_SERVER');
        $this->passEnvironment('MAIL_SMTP_PORT');
        $this->passEnvironment('MAIL_SMTP_USERNAME');

        $this->setEnvironment('IMAGE_PROCESSOR', 'ImageMagick');
        $this->setEnvironment('IMAGE_PROCESSOR_COLORSPACE', 'sRGB');
        $this->setEnvironment('IMAGE_PROCESSOR_PATH', '/usr/bin/');

        foreach ($options['php_environment'] as $name => $value) {
            if ($value !== null) {
                $this->setEnvironment($name, $value);
            } else {
                $this->passEnvironment($name);
            }
        }

        $this->addDb($options);
        $this->addPhpApache($options);
    }

    public function execute(array $options)
    {
        $compose = [
            'version' => '2',
            'services' => $this->services,
            'volumes' => $this->volumes,
        ];
        ksort($compose['services']);
        ksort($compose['volumes']);
        foreach ($compose['services'] as &$service) {
            ksort($service);
        }
        file_put_contents('docker-compose.yml', Yaml::dump($compose, 4, 2));


        $version = implode('.', array_slice(explode('.', $this->getPhpPackage()->getVersion()), 0, 2));
        $dockerfile = ["FROM chialab/php:$version-apache"];
        $dockerfile[] = $this->getDockerAptRun($options);
        $dockerfile[] = $this->getDockerConfigRun($options);
        file_put_contents(self::DOCKERFILE_PHP, implode("\n\n", $dockerfile));
        file_put_contents('.dockerignore', '*');
    }

    protected function addDb(array $options)
    {
        if ($this->environment['MYSQL_HOST'] !== 'db') {
            return; // seems like the db image isn't needed
        }

        if ($this->environment['MYSQL_USERNAME'] !== 'root') {
            throw new \RuntimeException("MySql users other than root aren't supported, got " . $this->environment['MYSQL_USERNAME']);
        }

        $this->defineService('db', [
            'image' => "mysql:{$options['db_version']}",
            'command' => 'mysqld'
                . ' --log-error-verbosity=2'
                . ' --sql-mode=NO_ENGINE_SUBSTITUTION'
                . ' --character-set-server=utf8'
                . ' --collation-server=utf8_general_ci',
            'volumes' => [
                'database:/var/lib/mysql'
            ],
            'ports' => [
                "{$options['db_port']}:3306",
            ],
            'environment' => [
                'MYSQL_ROOT_PASSWORD=' . $this->environment['MYSQL_PASSWORD'],
                'MYSQL_DATABASE=' . $this->environment['MYSQL_DATABASE'],
            ],
        ]);

        $this->addVolume('database');
    }

    protected function addPhpApache(array $options)
    {
        $service = [
            'build' => [
                'dockerfile' => self::DOCKERFILE_PHP,
                'context' => '.'
            ],
            'working_dir' => '/var/www',
            'volumes' => [
                '.:/var/www:delegated',
            ],
            'ports' => [
                "{$options['web_port']}:80",
            ],
            'links' => [
                'db',
            ],
            'user' => 'root:www-data',
            'environment' => [],
        ];

        // generate typo3temp
        if (true) {
            $typo3tempPath = Path::join('/var/www/', $options['web_dir'], 'typo3temp');
            $service['tmpfs'][] = $typo3tempPath . ':uid=33,gid=33,size=1G';
        }

        $this->defineService('php', $service);
    }

    protected function getDockerAptRun(array $options): string
    {
        $commands = [];

        // TODO allow post install commands (within and outside the first run command)

        if (version_compare($this->getPhpPackage()->getVersion(), '7.2', '<')) {
            $commands[] = 'echo deb http://deb.debian.org/debian stretch main >> /etc/apt/sources.list.d/stretch.list';
            $commands[] = 'echo "Package: libpcre3" >> /etc/apt/preferences.d/stretch';
            $commands[] = 'echo "Pin: release a=stretch" >> /etc/apt/preferences.d/stretch';
            $commands[] = 'echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/stretch';
            $commands[] = 'echo "Package: *" >> /etc/apt/preferences.d/stretch';
            $commands[] = 'echo "Pin: release a=stretch" >> /etc/apt/preferences.d/stretch';
            $commands[] = 'echo "Pin-Priority: 1" >> /etc/apt/preferences.d/stretch';
            $this->addPackage('libpcre3');
        }

        natcasesort($this->packages);

        $commands[] = 'apt-get update';
        $commands[] = 'DEBIAN_FRONTEND=noninteractive apt-get install -y ' . implode(' ', $this->packages);
        $commands[] = 'rm -r /var/lib/apt/lists/*';

        return 'RUN ' . implode(" \\\n && ", $commands);
    }

    protected function getDockerConfigRun(array $options): string
    {
        $commands = [];

        // change the document root
        $commands[] = "sed -i 's#/var/www/html#/var/www/{$options['web_dir']}#g' /etc/apache2/sites-enabled/000-default.conf";

        // add modules
        $commands[] = "a2enmod " . implode(" ", $this->apacheModules);

        $iniEntries = [];
        foreach (array_merge($iniEntries, $options['php_ini']) as $key => $value) {
            $commands[] = "echo " . escapeshellarg("$key=$value") . " >> /usr/local/etc/php/conf.d/php.ini";
        }

        return 'RUN ' . implode(" \\\n && ", $commands);
    }
}