<?php

namespace Hn\Typo3Environment\Generator;


use Composer\Package\PackageInterface;
use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class RealUrlGenerator implements GeneratorInterface
{
    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $this->container->getUnprepared(ExtensionGenerator::class)->preventAsking('hn_realurl');
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        if ($this->getRealUrlPackage() === null) {
            return;
        }

        if ($this->container->has(GitIgnoreGenerator::class)) {
            $gitignore = $this->container->get(GitIgnoreGenerator::class);
            $gitignore->addRule(Path::join($options['web_dir'], 'typo3conf/realurl_autoconf.php'));
        }

        if ($this->container->has(ExtConfGenerator::class)) {
            $extConf = $this->container->get(ExtConfGenerator::class);
            $extConf->addExtConf('realurl', 'autoConfFormat', '0', '1');
        }
    }

    /**
     * @return PackageInterface|null
     */
    protected function getRealUrlPackage()
    {
        $localRepository = $this->container->getComposer()->getRepositoryManager()->getLocalRepository();
        return $localRepository->findPackage('dmitryd/typo3-realurl', '^2.3');
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
    }
}