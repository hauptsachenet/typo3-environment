<?php

namespace Hn\Typo3Environment\Generator;


use Hn\Typo3Environment\Generator\Makefile\Target;
use Hn\Typo3Environment\GeneratorContainer;
use Hn\Typo3Environment\Utility\MarkerArea;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\PathUtil\Path;

class DeploymentGenerator implements GeneratorInterface
{
    const CORE_EXCLUDE_TABLES = [
        'be_sessions',
        'cf_cache_hash',
        'cf_cache_hash_tags',
        'cf_cache_pages',
        'cf_cache_pages_tags',
        'cf_cache_pagesection',
        'cf_cache_pagesection_tags',
        'fe_sessions',
    ];

    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var string[]
     */
    private $excludePaths = [
        '.git'
    ];

    /**
     * @var string[]
     */
    private $excludeTables = [];

    /**
     * @var array
     */
    private $sharedFolder = [];

    /**
     * @var MarkerArea
     */
    private $deployExcludeMarker;

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;
        $this->deployExcludeMarker = new MarkerArea(get_class($this));
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault(get_class($this), function (Options $options) {
            $question = "generate deployment targets in make file? (default yes) ";
            return $this->container->getIo()->askConfirmation($question, true);
        });

        $remoteResolver = new OptionsResolver();
        $remoteResolver->setRequired('branch');
        $remoteResolver->setRequired('host');
        $remoteResolver->setDefault('path', function (Options $options) {
            if (strpos($options['host'], 'mittwaldserver.info') !== false) {
                return 'html/master';
            }

            return '/var/www/master';
        });
        $remoteResolver->setDefault('php_cli', function (Options $options) {
            if (strpos($options['host'], 'mittwaldserver.info') !== false) {
                return 'php_cli';
            }

            return 'php';
        });
        $remoteResolver->setNormalizer('path', function (Options $options, $path) {
            return rtrim($path, '/');
        });

        $resolver->setDefault('deployments', []);
        $resolver->setNormalizer('deployments', function (Options $options, array $deployments) use ($remoteResolver) {
            foreach ($deployments as $branch => $deployment) {
                $deployments[$branch] = $remoteResolver->resolve(['branch' => $branch] + $deployment);
            }

            return $deployments;
        });

        $resolver->setDefault('deployment_test_urls', ['/']);
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        $this->prepareExcludeFiles($options);
        $this->prepareDeployCommand($options);
        $this->addSharedFolder(Path::join($options['web_dir'], 'fileadmin'), 'fileadmin');
        $this->addSharedFolder(Path::join($options['web_dir'], 'uploads'), 'uploads');

        if ($this->container->has(ConfigGenerator::class)) {
            $config = $this->container->get(ConfigGenerator::class);
            $pathToSharedConfiguration = Path::makeRelative(
                Path::join('shared/config/AdditionalConfiguration.php'),
                Path::join('current', $options['web_dir'], 'typo3conf')
            );

            $config->prependConfiguration(
                '// allow for including a shared configuration file.',
                '@include __DIR__ . ' . var_export('/' . $pathToSharedConfiguration, true) . ';'
            );
        }

        $this->preparePullCommand($options);
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
        sort($this->excludePaths);
        $this->deployExcludeMarker->replaceInFile('deploy-exclude.txt', implode(PHP_EOL, $this->excludePaths));
    }

    public function addExcludePath(string $path)
    {
        foreach ($this->excludePaths as $existingPath) {
            // if this exclude path is already covered by another rule ignore it
            if (Path::isBasePath($existingPath, $path)) {
                return;
            }

            // if another rule is more precise than the current one remove it
            if (Path::isBasePath($path, $existingPath)) {
                unset($this->excludePaths[$existingPath]);
            }
        }

        $this->excludePaths[$path] = $path;
    }

    public function addExcludeTable(string $table)
    {
        if (isset($this->excludeTables[$table])) {
            return;
        }

        $this->excludeTables[$table] = $table;
        $make = $this->container->get(MakefileGenerator::class);
        $make['pull']->appendEnvironment('PULL_EXCLUDE_TABLES', ',' . $table);
    }

    public function addSharedFolder(string $livePath, string $sharedPath)
    {
        $sharedFolder = Path::join('shared', $sharedPath);
        $this->sharedFolder[$livePath] = $sharedFolder;

        if ($this->container->has(GitIgnoreGenerator::class)) {
            $gitignore = $this->container->get(GitIgnoreGenerator::class);
            $gitignore->addRule($livePath);
        }

        $make = $this->container->get(MakefileGenerator::class);
        $make['clean']->addConditionalCommand('ifdef CLEAN_SHARED', 'rm -rf ' . escapeshellarg($livePath));

        $make['pull']->addCommand(
            'rsync ' . implode(' ', [
                '-rlte ' . escapeshellarg($this->getSshCommand('pull')),
                '--delete-after',
                ':' . rtrim(Path::join('$(PULL_PATH)', $sharedFolder), '/') . '/',
                rtrim($livePath, '/') . '/'
            ]),
            MakefileGenerator::POSITION_END
        );

        $deployedPath = Path::join('$(DATE)', $livePath);
        $this->addSshCommand('deploy',
            implode(" && \\\n\t", [
                'cd $(DEPLOY_PATH)',
                'mkdir -pv ' . escapeshellarg($sharedFolder),
                'rm -rvf ' . escapeshellarg($deployedPath),
                'ln -sf ' . escapeshellarg(Path::makeRelative($sharedFolder, dirname($deployedPath))) . ' ' . escapeshellarg($deployedPath)
            ]),
            MakefileGenerator::POSITION_TOP
        );
    }

    private function prepareExcludeFiles(array $options)
    {
        $installationManager = $this->container->getComposer()->getInstallationManager();
        $localRepository = $this->container->getComposer()->getRepositoryManager()->getLocalRepository();
        $packages = $localRepository->getCanonicalPackages();
        foreach ($packages as $package) {
            $installPath = Path::makeRelative($installationManager->getInstallPath($package), getcwd());

            // the global package repository does not contain information about autoload-dev dirs
            // for that reason, i need to load the json file within the repository
            $jsonPath = Path::join($installPath, 'composer.json');
            if (file_exists($jsonPath)) {
                $composer = json_decode(file_get_contents($jsonPath), true);
                if (isset($composer['autoload-dev'])) {
                    foreach ($composer['autoload-dev'] as $type => $paths) {
                        foreach ($paths as $path) {
                            $this->addExcludePath(Path::join($installPath, $path));
                        }
                    }
                }
            }
        }
    }

    private function preparePullCommand(array $options)
    {
        foreach (self::CORE_EXCLUDE_TABLES as $excludeTable) {
            $this->addExcludeTable($excludeTable);
        }

        $make = $this->container->get(MakefileGenerator::class);
        $defaultRemote = $options['deployments'] ? reset($options['deployments']) : [];

        $make['.PHONY']->addDependency($make['pull']);
        $make['pull']->addDependency($make['start']);

        $make['pull']->setEnvironment('PULL_PATH', $defaultRemote['path'] ?? null);
        $make['pull']->setEnvironment('PULL_PHP', $defaultRemote['php_cli'] ?? 'php');
        $make['pull']->setEnvironment('PULL_TYPO3CMS', '$(PULL_PHP) $(PULL_PATH)/current/vendor/bin/typo3cms');

        $dumpCommand = '$(PULL_TYPO3CMS) database:export --exclude-tables $(PULL_EXCLUDE_TABLES)';

        $make['pull']->setEnvironment('LOCAL_TYPO3CMS_PIPEABLE', 'docker exec -i `docker-compose ps -q php` vendor/bin/typo3cms');
        $transferDbCommand = $this->getSshCommand('pull', $dumpCommand) . ' |$(LOCAL_TYPO3CMS_PIPEABLE) database:import';
        $this->addCommand('pull', $transferDbCommand, MakefileGenerator::POSITION_TOP);
        $this->addCommand('pull', '$(LOCAL_TYPO3CMS) database:updateschema --verbose', MakefileGenerator::POSITION_TOP);

        if ($this->container->has(DockerGenerator::class)) {
            $docker = $this->container->get(DockerGenerator::class);
            $docker->addPackage('mysql-client');
        }
    }

    /**
     * @param array $options
     */
    private function prepareDeployCommand(array $options)
    {
        $make = $this->container->get(MakefileGenerator::class);
        $defaultRemote = $options['deployments'] ? reset($options['deployments']) : [];

        $make['.PHONY']->addDependency($make['deploy']);
        $make['deploy']->addDependency($make['install']);
        $make['deploy']->addCommandForEnvironment('deployment',
            '',
            '$(error You must be in a deployment environment (ENVIRONMENT=deployment))',
            MakefileGenerator::POSITION_INIT
        );

        $make['deploy']->setEnvironment('DATE', '$(shell date +%Y-%m-%d-%H-%M-%S)');
        $make['deploy']->setEnvironment('DEPLOY_PATH', $defaultRemote['path'] ?? null);
        $make['deploy']->setEnvironment('DEPLOY_PHP', $defaultRemote['php_cli'] ?? 'php');
        $make['deploy']->setEnvironment('DEPLOY_TYPO3CMS', '$(DEPLOY_PHP) $(DEPLOY_PATH)/$(DATE)/vendor/bin/typo3cms');

        // create initial folder
        $this->addSshCommand('deploy', 'mkdir -pv $(DEPLOY_PATH)/$(DATE)', MakefileGenerator::POSITION_TOP);

        $parameters = [
            '-rlcze ' . escapeshellarg($this->getSshCommand('deploy')),
            '--copy-dest=`' . $this->getSshCommand('deploy', 'readlink -f $(DEPLOY_PATH)/current') . '`',
            '--exclude-from="deploy-exclude.txt"'
        ];
        // FIXME this may not work with the web dir "."
        $parameters[] = "{$options['web_dir']} vendor :$(DEPLOY_PATH)/$(DATE)";
        $make['deploy']->addCommand("rsync " . implode(" \\\n\t", $parameters), MakefileGenerator::POSITION_TOP);

        // run upgrade commands
        $this->addSshCommand('deploy', '$(DEPLOY_TYPO3CMS) database:updateschema --verbose');
        $this->addSshCommand('deploy', '$(DEPLOY_TYPO3CMS) extension:setupactive');
        // $this->addSshCommand('deploy', '$(DEPLOY_TYPO3CMS) upgrade:all --verbose');

        // release
        $this->addSshCommand('deploy', 'rm -f $(DEPLOY_PATH)/current && ln -s $(DATE) $(DEPLOY_PATH)/current', MakefileGenerator::POSITION_END);
        $this->addSshCommand('deploy', '$(DEPLOY_PHP) $(DEPLOY_PATH)/$(DATE)/vendor/bin/typo3cms cache:flush', MakefileGenerator::POSITION_END);

        // test requests
        foreach ($options['deployment_test_urls'] as $url) {
            $this->addSshCommand('deploy', '$(DEPLOY_TYPO3CMS) frontend:request ' . escapeshellarg($url), MakefileGenerator::POSITION_END);
        }

        // remove old releases
        $cleanupCommand = 'find $(DEPLOY_PATH) -maxdepth 1 -type d -regextype sed -regex ".*/[0-9-]\{19\}" |sort |head -n-5 |xargs rm -rf';
        $this->addSshCommand('deploy', $cleanupCommand, MakefileGenerator::POSITION_END);
    }

    private $ssh = [];

    public function getSshCommand(string $target, string ...$commands)
    {
        $make = $this->container->get(MakefileGenerator::class);
        if (!isset($this->ssh[$target])) {
            $deployments = $this->container->getOption('deployments');
            $defaultRemote = $deployments ? reset($deployments) : [];
            $socketName = strtoupper($target) . '_SOCKET';
            $hostName = strtoupper($target) . '_HOST';
            $make[$target]->setEnvironment($socketName, '/tmp/ssh_deploy_$(notdir $(CURDIR))');
            $make[$target]->setEnvironment($hostName, $defaultRemote['host'] ?? null);
            $this->ssh[$target] = self::createSshCommand($make[$target], "$($socketName)", "$($hostName)");
        }

        return $this->ssh[$target](...$commands);
    }

    public function addCommand(string $target, string $command, int $priority = 0)
    {
        $make = $this->container->get(MakefileGenerator::class);
        $make[$target]->addCommand($command, $priority);
    }

    public function addSshCommand(string $target, string $command, int $priority = 0)
    {
        $make = $this->container->get(MakefileGenerator::class);
        $make[$target]->addCommand($this->getSshCommand($target, $command), $priority);
    }

    private static function createSshCommand(Target $target, string $socket, string $host): \Closure
    {
        $target->addCommand("if [ -e $socket ]; then ssh -O exit -o ControlPath=$socket -; fi", MakefileGenerator::POSITION_INIT);
        $target->addCommand("ssh -nNf -o ControlMaster=yes -o ControlPath=$socket -o ControlPersist=3600 $host", MakefileGenerator::POSITION_INIT);
        $target->addCommand("ssh -O exit -o ControlPath=$socket -", MakefileGenerator::POSITION_CLEANUP);

        return function (string ...$commands) use ($target, $socket): string {
            $base = "ssh -o ControlPath=$socket";
            $commandStr = implode(" && \\\n\t", $commands);
            return empty($commands) ? $base : "$base - " . escapeshellarg($commandStr);
        };
    }
}