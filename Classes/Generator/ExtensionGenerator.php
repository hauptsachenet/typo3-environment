<?php

namespace Hn\Typo3Environment\Generator;


use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Package\PackageInterface;
use Hn\Typo3Environment\GeneratorContainer;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Webmozart\PathUtil\Path;

class ExtensionGenerator implements GeneratorInterface
{
    const PHP_PREFIX = "<?php\n\n";
    const PHP_CONF_PREFIX = "<?php\ndefined('TYPO3_MODE') or die();\n\n";

    /**
     * @var GeneratorContainer
     */
    private $container;

    /**
     * @var array
     */
    private $extensions = [];

    /**
     * @var array
     */
    private $askExtensions = [];

    /**
     * @var array
     */
    private $preventAsking = [];

    /**
     * @var bool
     */
    private $optionsConfigured = false;

    public function askForExtension(string $extKey, bool $default = true)
    {
        if ($this->optionsConfigured) {
            throw new \RuntimeException("It is to late to ask for extensions installation. This must be called during configureOptions");
        }

        $this->askExtensions[$extKey] = $default || ($this->askExtensions[$extKey] ?? false);
    }

    public function preventAsking(string $extKey)
    {
        $this->preventAsking[$extKey] = true;
    }

    /**
     * @param GeneratorContainer $container
     */
    public function __construct(GeneratorContainer $container)
    {
        $this->container = $container;

        $installationManager = $this->container->getComposer()->getInstallationManager();
        foreach ($this->getLocalInstalledExtensions() as $package) {
            $extKey = basename($installationManager->getInstallPath($package));
            $localExtKey = $this->getLocalExtKey($extKey);

            $emconf = $this->readEmConf($extKey);
            $default = isset($emconf['category']) && in_array($emconf['category'], ['fe', 'plugin']);
            $this->askForExtension($localExtKey, $default);
        }
    }

    /**
     * @return \Iterator|PackageInterface[]
     */
    protected function getLocalInstalledExtensions(): \Iterator
    {
        $localRepository = $this->container->getComposer()->getRepositoryManager()->getLocalRepository();
        $installationManager = $this->container->getComposer()->getInstallationManager();

        foreach ($localRepository->getCanonicalPackages() as $package) {
            if ($package->getType() !== 'typo3-cms-extension') {
                continue;
            }

            // sanity check: is this extension going to be installed in the typo3conf/ext folder
            $installPath = Path::makeRelative($installationManager->getInstallPath($package), getcwd());
            if (strpos($installPath, 'typo3conf/ext') === false) {
                continue;
            }

            yield $package;
        }
    }

    protected function getLocalExtKey(string $extKey): string
    {
        return 'hn_' . $extKey;
    }

    protected function readEmConf(string $extKey): array
    {
        $EM_CONF = [];
        $_EXTKEY = $extKey;
        /** @noinspection PhpIncludeInspection */
        @include Path::join($this->container->getWebDir(), 'typo3conf/ext', $extKey, 'ext_emconf.php');
        return $EM_CONF[$_EXTKEY] ?? [];
    }

    /**
     * Allows interaction with the user and access to the configuration
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('generate_local_extension', []);
        $resolver->setNormalizer('generate_local_extension', function (Options $options, array $extensions) {
            foreach ($this->askExtensions as $extKey => $default) {
                if (isset($extensions[$extKey])) {
                    continue;
                }

                if ($this->preventAsking[$extKey] ?? false) {
                    continue;
                }

                // if there is already a local version than don't ask
                $extDir = Path::join($this->container->getWebDir(), 'typo3conf/ext', $extKey);
                if (is_dir($extDir)) {
                    continue;
                }

                $defaultSpelled = $default ? 'yes' : 'no';
                $msg = "Generate an extension <info>$extKey</info>? (default <info>$defaultSpelled</info>) ";
                if (!$this->container->getIo()->askConfirmation($msg, $default)) {
                    $extensions[$extKey] = false;
                }
            }

            return $extensions;
        });
    }

    protected static function put($filename, $content)
    {
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0777, true);
        }

        file_put_contents($filename, $content);
    }

    protected function addExtension(string $extKey)
    {
        if (isset($this->extensions[$extKey])) {
            return;
        }

        $this->extensions[$extKey] = [
            'generate' => true,
            'ext_emconf' => [
                'title' => $extKey,
                'descriptions' => 'local ' . $extKey,
                'category' => 'misc',
                'constraints' => [
                    'depends' => [],
                    'conflicts' => [],
                    'suggests' => [],
                ],
                'state' => 'excludeFromUpdates',
                'uploadfolder' => false,
                'createDirs' => '',
                'clearCacheOnLoad' => true,
                'author' => '',
                'author_email' => '',
                'author_company' => '',
            ],
            'ext_localconf' => [],
            'ext_tables' => [],
            'ts_setup' => [],
            'ts_const' => [],
            'ts_config' => [],
            'templates' => [],
            'partials' => [],
            'layouts' => [],
        ];
    }

    public function hintExtension(string $extKey, string $hint, $value)
    {
        $this->addExtension($extKey);

        if (!isset($this->extensions[$extKey][$hint])) {
            $supportedHints = implode(', ', array_keys($this->extensions[$extKey]));
            throw new \RuntimeException("Hint $hint not supported, only supported are $supportedHints.");
        }

        switch ($hint) {
            case "generate":
                $this->extensions[$extKey][$hint] = (bool)$value;
                break;
            case "ext_emconf":
                $this->extensions[$extKey][$hint] = array_replace_recursive($this->extensions[$extKey][$hint], $value);
                break;
            case "ext_localconf":
            case "ext_tables":
            case "ts_setup":
            case "ts_const":
            case "ts_config":
                array_push($this->extensions[$extKey][$hint], ...$value);
                break;
            case "templates":
            case "partials":
            case "layouts":
                $this->extensions[$extKey][$hint] = array_replace($this->extensions[$extKey][$hint], $value);
                break;
            default:
                throw new \RuntimeException("Hint '$hint' not implemented");
        }
    }

    /**
     * Prepare the interaction with other generators
     *
     * @param array $options
     */
    public function prepare(array $options)
    {
        // consider generating a local extension for each composer extension
        $installationManager = $this->container->getComposer()->getInstallationManager();
        foreach ($this->getLocalInstalledExtensions() as $extension) {
            $extKey = basename($installationManager->getInstallPath($extension));
            $localExtKey = $this->getLocalExtKey($extKey);

            // check if this extension is blacklisted
            if ($this->preventAsking[$extKey] ?? false) {
                continue;
            }

            // check if this extension was explicitly disabled
            $installExtension = $options['generate_local_extension'][$localExtKey] ?? true;
            if (!$installExtension) {
                continue;
            }

            $sourceEmConf = $this->readEmConf($extKey);

            $this->hintExtension($localExtKey, 'ext_emconf', [
                'category' => $sourceEmConf['category'] ?? 'misc',
                'constraints' => [
                    'depends' => [
                        $extKey => '*'
                    ]
                ]
            ]);

            $txName = str_replace('_', '', $extKey);
            $this->hintExtension($localExtKey, 'ts_setup', [
                "plugin.tx_$txName {",
                "    views {",
                "        templateRootPaths.10 = EXT:$localExtKey/Resources/Private/Templates/",
                "        partialRootPaths.10 = EXT:$localExtKey/Resources/Private/Partials/",
                "        layoutRootPaths.10 = EXT:$localExtKey/Resources/Private/Layouts/",
                "    }",
                "}",
            ]);
        }

        // also generate extensions which are defined additionally
        foreach ($options['generate_local_extension'] as $extKey => $install) {
            $this->hintExtension($extKey, 'generate', $install);

            $txName = str_replace('_', '', $extKey);
            $this->hintExtension($extKey, 'ts_setup', [
                "plugin.tx_$txName {",
                "    views {",
                "        templateRootPaths.0 = EXT:$extKey/Resources/Private/Templates/",
                "        partialRootPaths.0 = EXT:$extKey/Resources/Private/Partials/",
                "        layoutRootPaths.0 = EXT:$extKey/Resources/Private/Layouts/",
                "    }",
                "}",
            ]);
        }
    }

    /**
     * Actually generate what is supposed to be generated
     *
     * @param array $options
     */
    public function execute(array $options)
    {
        $generatedExtensions = 0;

        foreach ($this->extensions as $extKey => $hints) {
            if (!$hints['generate']) {
                continue;
            }

            $extPath = Path::join($this->container->getWebDir(), 'typo3conf/ext', $extKey);
            if (file_exists($extPath)) {
                continue;
            }

            self::put(
                Path::join($extPath, 'ext_emconf.php'),
                self::PHP_PREFIX . '$EM_CONF[$_EXTKEY] = ' . var_export($hints['ext_emconf'], true) . ';'
            );
            self::put(
                Path::join($extPath, 'ext_localconf.php'),
                self::PHP_CONF_PREFIX . implode(PHP_EOL, array_merge(
                    [
                        ExtensionManagementUtility::class . "::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:$extKey/Configuration/TSconfig/tsconfig.txt\">');"
                    ],
                    $hints['ext_localconf']
                ))
            );
            self::put(
                Path::join($extPath, 'ext_tables.php'),
                self::PHP_CONF_PREFIX . implode(PHP_EOL, array_merge(
                    [
                        ExtensionManagementUtility::class . "::addStaticFile('$extKey', 'Configuration/TypoScript', '$extKey configuration');",
                    ],
                    $hints['ext_tables']
                ))
            );

            self::put(
                Path::join($extPath, 'Configuration/TypoScript/setup.txt'),
                implode(PHP_EOL, $hints['ts_setup'])
            );
            self::put(
                Path::join($extPath, 'Configuration/TypoScript/constants.txt'),
                implode(PHP_EOL, $hints['ts_const'])
            );

            self::put(
                Path::join($extPath, 'Configuration/TSconfig/tsconfig.txt'),
                implode(PHP_EOL, $hints['ts_config'])
            );

            foreach (['templates', 'partials', 'layouts'] as $type) {
                foreach ($hints[$type] as $path => $template) {
                    $fullPath = Path::join($extPath, 'Resources/Private', ucfirst($type), $path);
                    self::put($fullPath, $template);
                }
            }

            $composerFile = new JsonFile(trim(getenv('COMPOSER')) ?: 'composer.json', null, $this->container->getIo());
            $configSource = new JsonConfigSource($composerFile);
            $namespace = 'Hn\\' . GeneralUtility::underscoredToUpperCamelCase($extKey) . '\\';

            $t3Env = $this->container->getComposer()->getRepositoryManager()->getLocalRepository()->findPackage('hn/typo3-environment', '*');
            $autoloadKey = $t3Env->isDev() ? 'autoload-dev' : 'autoload';
            $autoloadDefinitions = array_replace_recursive(
                $composerFile->read()[$autoloadKey] ?? [],
                ['psr-4' => [$namespace => Path::join($extPath, 'Classes')]]
            );
            foreach ($autoloadDefinitions as &$autoloadDefinition) {
                asort($autoloadDefinition);
            }
            $configSource->addProperty($autoloadKey, $autoloadDefinitions);

            ++$generatedExtensions;
        }

        if ($generatedExtensions > 0) {
            if ($generatedExtensions === 1) {
                $msg = "<info>1</info> extensions generated";
            } else {
                $msg = "<info>$generatedExtensions</info> extensions generated";
            }
            $msg .= " <warning>(You'll need to re-run composer for the autoload to take effect.)</warning>";
            $this->container->getIo()->write($msg);
        }
    }
}