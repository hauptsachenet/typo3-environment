<?php

namespace Hn\Typo3Environment\Generator\Makefile;


class Target
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Target[]
     */
    private $dependencies = [];

    /**
     * @var string[]
     */
    private $commands = [];

    /**
     * @var string[]
     */
    private $environment = [];

    /**
     * @var string[]
     */
    private $requirements = [];

    /**
     * Target constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Target $dependency
     */
    public function addDependency(Target $dependency)
    {
        $this->dependencies[$dependency->getName()] = $dependency;
    }

    /**
     * @return Target[]
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    /**
     * @param string $command
     * @param int $priority
     */
    public function addCommand(string $command, int $priority = 0)
    {
        $this->addConditionalCommand('', $command, '', $priority);
    }

    /**
     * @param string $condition
     * @param string $then
     * @param string $else
     * @param int $priority
     */
    public function addConditionalCommand(string $condition, string $then, string $else = '', int $priority = 0)
    {
        $this->commands[] = [
            'priority' => $priority,
            'condition' => $condition,
            'command' => $then,
            'else' => $else,
            'index' => count($this->commands)
        ];
    }

    /**
     * @param string $environment
     * @param string $command
     * @param string $else
     * @param int $priority
     */
    public function addCommandForEnvironment(string $environment, string $command, string $else = '', int $priority = 0)
    {
        $this->addConditionalCommand("ifeq ($(ENVIRONMENT), $environment)", $command, $else, $priority);
    }

    /**
     * @return string[]
     */
    public function getCommands(): array
    {
        return array_column($this->getDetailedCommands(), 'command');
    }

    /**
     * @return array
     */
    public function getDetailedCommands(): array
    {
        // sort commands in a stable way (keep the order the same if the priority is the same)
        // the higher the priority, the further up the command so this is a descending sort
        usort($this->commands, function (array $a, array $b): int {
            return $b['priority'] <=> $a['priority']
                ?: $a['index'] <=> $b['index'];
        });

        return $this->commands;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setEnvironment(string $name, string $value = null)
    {
        if ($value === null) {
            $this->setEnvironmentRequired($name);
        } else {
            $this->environment["$name?"] = $value;
        }
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function appendEnvironment(string $name, string $value)
    {
        if (isset($this->environment["$name:"])) {
            $this->environment["$name:"] .= $value;
        } else {
            $this->environment["$name:"] = $value;
        }
    }

    /**
     * @return string[]
     */
    public function getEnvironment(): array
    {
        return $this->environment;
    }

    /**
     * @param string $name
     * @param string $message
     */
    public function setEnvironmentRequired(string $name, string $message = null)
    {
        $this->requirements[$name] = $message ?: "environment $name is not defined";
    }

    /**
     * @return string[]
     */
    public function getEnvironmentRequirement(): array
    {
        return $this->requirements;
    }
}